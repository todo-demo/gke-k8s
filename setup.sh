#!/bin/bash
set -eu

echo " - Deploy cluster"
terraform apply -auto-approve

echo " - Setup kubeconfig"
gcloud container clusters get-credentials tf-gke-k8s --zone us-west1-b --project dynamic-demo-678945

echo " - Cluster"
kubectl get nodes

echo " - Gitlab Configuration"
kubectl apply -f kube/gitlab-service-account.yaml
cluster_ip="https://$(gcloud container clusters list | grep tf-gke-k8s | awk '{split($0,a," "); print a[4]}')"
cluster_certificate=$(kubectl get secret $(kubectl get secret | grep "default-token" | cut -f1 -d ' ') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode)
cluster_token=$(kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') | grep 'token:' | awk '{print $2}')

echo "    - Endpoint: " $cluster_ip
echo "    - Certificat: " $cluster_certificate
echo "    - Token: " $cluster_token

formated_cert=$(echo "-----BEGIN CERTIFICATE-----\r\n"$(echo $cluster_certificate | sed 's/-----BEGIN CERTIFICATE----- //g' | sed 's/ -----END CERTIFICATE-----//g' | sed 's/ /\\r\\n/g')"\r\n-----END CERTIFICATE-----")

curl -H "Private-Token: $GITLAB_TOKEN" -H "Accept: application/json" -H "Content-Type:application/json" \
     -X POST -d $'{"name":"GKE CLuster", "platform_kubernetes_attributes": {"api_url": "'$cluster_ip'", "token": "'"$cluster_token"'", "ca_cert": "'"$formated_cert"'"}}' \
     https://gitlab.com/api/v4/groups/7299497/clusters/user

echo " - Setup Gitlab Registry Credentials Secret"
kubectl create secret -n "default" \
      docker-registry gitlab-registry \
      --docker-server="https://registry.gitlab.com/v1/" \
      --docker-username="brice.santus@gmail.com" \
      --docker-password=$(bw get password gitlab.com) \
      --docker-email="brice.santus@gmail.com" \
      -o yaml
