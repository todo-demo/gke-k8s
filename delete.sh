set -eu


echo "\n - Delete Gitlab integration"
cluster_id=$(curl -s --header "Private-Token: $GITLAB_TOKEN" https://gitlab.com/api/v4/groups/7299497/clusters | jq '.[].id')
echo "Delete cluster $cluster_id"
curl --request DELETE --header "Private-Token: $GITLAB_TOKEN" https://gitlab.com/api/v4/groups/7299497/clusters/$cluster_id

echo "\n - Depro cluster"
terraform destroy -auto-approve
