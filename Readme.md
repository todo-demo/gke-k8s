# Setup GKE Cluster

> Based on https://github.com/GoogleCloudPlatform/terraform-google-examples/tree/master/example-gke-k8s-service-lb

## Getting started

* Install:
  * **terraform** : https://www.terraform.io/docs/cli-index.html
  * **gcloud** : https://cloud.google.com/sdk/docs?hl=fr#deb
  * **kubectl** : https://kubernetes.io/fr/docs/tasks/tools/install-kubectl/#installer-kubectl-sur-linux


## GCloud API

Will need to activate the follow API on the projet:

* Compute Engine API
* Kubernetes Engine API

## Setup GCloud Credentials

```bash
~/workspace/perso/gke-k8s master
❯ gcloud init

~/workspace/perso/gke-k8s master
❯ gcloud projects create dynamic-demo-678945
Create in progress for [https://cloudresourcemanager.googleapis.com/v1/projects/dynamic-demo-678945].
Waiting for [operations/cp.7437193928740956769] to finish...done.
Enabling service [cloudapis.googleapis.com] on project [dynamic-demo-678945]...
Operation "operations/acf.bd6a4188-f7ca-4705-a0e5-a1835dd46ba2" finished successfully.

~/workspace/perso/gke-k8s master
❯ gcloud config set project dynamic-demo-678945
Updated property [core/project].
```
```bash
~/workspace/perso/gke-k8s master
❯ [[ $CLOUD_SHELL ]] || gcloud auth application-default login
Your browser has been opened to visit:

    https://accounts.google.com/o/oauth2/auth?code_challenge=I3L4EpjSfCc0KT-QYQMcsG7qtZulwBWNSRBIEtzZizQ&prompt=select_account&code_challenge_method=S256&access_type=offline&redirect_uri=http%3A%2F%2Flocalhost%3A8085%2F&response_type=code&client_id=764086051850-6qr4p6gpi6hn506pt8ejuq83di341hur.apps.googleusercontent.com&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcloud-platform+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Faccounts.reauth


Opening in existing browser session.

Credentials saved to file: [/home/bsantus/.config/gcloud/application_default_credentials.json]

These credentials will be used by any library that requests Application Default Credentials (ADC).

~/workspace/perso/gke-k8s master
❯ export GOOGLE_PROJECT=$(gcloud config get-value project)

~/workspace/perso/gke-k8s master
❯ echo $GOOGLE_PROJECT
dynamic-demo-678945

```

## Setup Cluster

```bash
~/workspace/perso/gke-k8s master # Get the kubectl credentials
❯ terraform validate && terraform apply
[...]

~/workspace/perso/gke-k8s master # Get the kubectl credentials
❯ gcloud container clusters get-credentials tf-gke-k8s --zone us-west1-b --project dynamic-demo-678945
```

## Check Cluster

```bash
~/workspace/perso/gke-k8s master*
❯ k get nodes
NAME                                        STATUS   ROLES    AGE     VERSION
gke-tf-gke-k8s-default-pool-29c656ae-33rp   Ready    <none>   2m24s   v1.14.10-gke.17
gke-tf-gke-k8s-default-pool-29c656ae-95gb   Ready    <none>   2m24s   v1.14.10-gke.17
gke-tf-gke-k8s-default-pool-29c656ae-xbgz   Ready    <none>   2m24s   v1.14.10-gke.17
```


## Gitlab integration

* Kubernetes configuration
```bash
~/workspace/perso/gke-k8s master # Get API Endpoint
❯ kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
```
```bash
~/workspace/perso/gke-k8s master # Get Certificat Endpoint
❯ kubectl get secret $(kubectl get secret | grep "default-token" | cut -f1 -d ' ') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
```
```bash
~/workspace/perso/gke-k8s master # Create Gitlab Service Account
❯ kubectl apply -f kube/gitlab-service-account.yaml

~/workspace/perso/gke-k8s master # Get the Gitlab SA Token
❯ kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}') | grep 'token:' | awk '{print $2}'
```
* Setup Gitlab managed apps
  * Helm/Tiller
  * Ingress
  * Gitlab Runner
* Setup AutoDevops with the xip.io domain

## AutoDevops
